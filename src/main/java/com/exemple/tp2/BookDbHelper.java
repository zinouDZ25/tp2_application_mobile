package com.exemple.tp2;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class BookDbHelper extends SQLiteOpenHelper {
    /**
     * Created By LAKHDARI ZINE EDDINE TD4
     */

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION =38;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";
    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";



    public BookDbHelper(MainActivity context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    // For The BookActivity
    public BookDbHelper(BookActivity context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        //create the table library
         db.execSQL("CREATE TABLE library (_id integer primary key autoincrement ," +
                 "title text not null,authors text,year text,genres text,publisher text)");

            }

            //drop the table if we need to create another version of this table
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" drop table if exists "+TABLE_NAME);
        onCreate(db);
    }


   /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
   //Create a Book using the ContentValues
    public boolean addBook(Book book) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_BOOK_TITLE, book.getTitle());
            values.put(COLUMN_AUTHORS, book.getAuthors());
            values.put(COLUMN_YEAR, book.getYear());
            values.put(COLUMN_GENRES, book.getGenres());
            values.put(COLUMN_PUBLISHER, book.getPublisher());
            db.insert(TABLE_NAME, null, values);
            db.close();
            return (true);
            }catch(Exception e){

            return false;
            }

     }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    //Update the book using the id of this book
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(_ID,book.getId());
        cv.put(COLUMN_BOOK_TITLE, book.getTitle());
        cv.put(COLUMN_AUTHORS, book.getAuthors());
        cv.put(COLUMN_YEAR, book.getYear());
        cv.put(COLUMN_GENRES, book.getGenres());
        cv.put(COLUMN_PUBLISHER, book.getPublisher());

        int res = 0;
        // call db.update()
        res=db.update(TABLE_NAME, cv, "_id="+book.getId(), null);
        // updating row return number of rows Updated !
        return res;
    }

    /**
     * Fetch all the books form the database
     * @return Cursor
     */

    public Cursor fetchAllBooks() {

        SQLiteDatabase db = this.getReadableDatabase();
        //Table with diffrent informations of the book
        String [] cols= new String[] {_ID,COLUMN_BOOK_TITLE,COLUMN_AUTHORS,COLUMN_YEAR,COLUMN_GENRES,COLUMN_PUBLISHER};
        Cursor mCursor =db.query(TABLE_NAME,cols,null,null,null,null,null,null);
        if(mCursor!=null){
            mCursor.moveToFirst();
        }
        return mCursor;
     }

    /**
     * fetch a book from the ID
     * @param id
     * @return Cursor
     */
    public Cursor fetchBook(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        String [] cols= new String[] {_ID,COLUMN_BOOK_TITLE,COLUMN_AUTHORS,COLUMN_YEAR,COLUMN_GENRES,COLUMN_PUBLISHER};
         Cursor mCursor =db.query(TABLE_NAME,cols,"_id=?",new String[] {id},null,null,null,null);
        if(mCursor!=null){
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Fetch book from the database using  Name ( title )
     * @param name
     * @return Cursor
     */
    public Cursor fetchBookName(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        String [] cols= new String[] {_ID,COLUMN_BOOK_TITLE,COLUMN_AUTHORS,COLUMN_YEAR,COLUMN_GENRES,COLUMN_PUBLISHER};
        Cursor mCursor =db.query(TABLE_NAME,cols,"title=?",new String[] {name},null,null,null,null);
        if(mCursor!=null){
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Delete the book using ID
     * @param id
     */
    public void deleteBook(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
         // call db.delete();
        db.delete(TABLE_NAME,"_id=?",new String[]{id});
        db.close();
    }


    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    /**
     * Convert Cursor To Book
     * @param cursor
     * @return Book
     */
    public static Book cursorToBook(Cursor cursor) {
        Book book = null;
	// build a Book object from cursor
        try
        {



            int idIndex = cursor.getColumnIndexOrThrow("_id");
            int titleIndex = cursor.getColumnIndexOrThrow("title");
            int authorsIndex = cursor.getColumnIndexOrThrow("authors");
            int yearIndex = cursor.getColumnIndexOrThrow("year");
            int genresIndex = cursor.getColumnIndexOrThrow("genres");
            int publisherIndex = cursor.getColumnIndexOrThrow("publisher");

            long id = cursor.getLong(idIndex);
            String title = cursor.getString(titleIndex);
            String authors = cursor.getString(authorsIndex);
            String year = cursor.getString(yearIndex);
            String genres = cursor.getString(genresIndex);
            String publisher = cursor.getString(publisherIndex);
            book = new Book(id,title,authors,year,genres,publisher);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return book;
    }
}
