package com.exemple.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends AppCompatActivity {
    public static BookDbHelper BookDbHelper;

    /**
     * Created By LAKHDARI ZINE EDDINE TD4
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Create the DBHelper to use it to connect with de database
        BookDbHelper = new BookDbHelper(this);
         //Read the data
        final SQLiteDatabase db = BookDbHelper.getReadableDatabase();
        BookDbHelper.onUpgrade(db,38,38);
        //Add the data of books in the Database
        BookDbHelper.populate();
        //Fetch all books from the table library
        Cursor cursor =BookDbHelper.fetchAllBooks();
        //The simpleCursorAdapter with the title of the book and the authors .
       final SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] { "title","authors" },
                new int[] { android.R.id.text1,android.R.id.text2 });

        ListView  listView = (ListView) findViewById(R.id.listBook);
        //set the data in the list View
        listView.setAdapter(dataAdapter);
        //set On item click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Position +1 because the first position in the database started from 1
                int positionD=position+1;
                //read
                SQLiteDatabase db = BookDbHelper.getReadableDatabase();
                //fetch for a book from the position Item selected
                Cursor cursor =BookDbHelper.fetchBook(String.valueOf(positionD));
                //Create the book from the Cursor
                Book book = BookDbHelper.cursorToBook(cursor);
                // show toast the id of this book
                Toast.makeText(MainActivity.this,
                       ""+book.getId(), Toast.LENGTH_LONG).show();
                //Create Intent to go to the second ativity BookActivity and send the Object book => myCustomerObj
                Intent intent = new Intent(MainActivity.this,BookActivity.class).putExtra("myCustomerObj",book);
                intent .putExtra("AddBook",false);
                //Start the Second Activity
                startActivity(intent);
            }
        });
        //Menu to Delete the book from listView
        listView.setOnCreateContextMenuListener(
                new View.OnCreateContextMenuListener() {

                    public void onCreateContextMenu(ContextMenu menu, View view,
                                                    ContextMenu.ContextMenuInfo menuInfo) {
                      final  AdapterView.AdapterContextMenuInfo mi = (AdapterView.AdapterContextMenuInfo) menuInfo;
                        menu.add(0, 0, 0, "Supprimer");
                        //if the user select << Supprimer >> then show a toast and delete the book from the Database and Refresh ListView
                        menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Toast.makeText(MainActivity.this, ""+mi.position, Toast.LENGTH_SHORT).show();
                                BookDbHelper.deleteBook(String.valueOf(mi.position+1));
                                onRestart();

                                return true;
                            }


                        });

                }
                });

        //Add A Book to The Database and ListView
        //Floating Button
        FloatingActionButton ajouterB = (FloatingActionButton)findViewById(R.id.addBookFloat);

        ajouterB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start the BookActivity to Add the Book
                Intent intent2 = new Intent(MainActivity.this,BookActivity.class).putExtra("AddBook",true);
                startActivity(intent2);
            }
        });

    }
    @Override
    public void onRestart() {
        super.onRestart();
        //When BACK BUTTON is pressed, the activity on the stack is restarted
        //Do what you want on the refresh procedure here
        Cursor cursor =BookDbHelper.fetchAllBooks();
        //The simpleCursorAdapter with the title of the book and the authors .
        final SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] { "title","authors" },
                new int[] { android.R.id.text1,android.R.id.text2 });
        ListView  listView = (ListView) findViewById(R.id.listBook);
        listView.setAdapter(dataAdapter);
    }
}
