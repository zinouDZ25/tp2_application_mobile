package com.exemple.tp2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    /**
     * Created By LAKHDARI ZINE EDDINE TD4
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        Book customerObjInToClass= new Book();
        final EditText name =(EditText) findViewById(R.id.nameBook);
        final EditText authors=(EditText)findViewById(R.id.editAuthors);
        final EditText year=(EditText)findViewById(R.id.editYear);
        final EditText genre=(EditText)findViewById(R.id.editGenres);
        final EditText publisher=(EditText)findViewById(R.id.editPublisher);

        if(getIntent().getExtras().getBoolean("AddBook")==false){
            //get the Book from the Clicked item
            customerObjInToClass = getIntent().getExtras().getParcelable("myCustomerObj");
            name.setText(customerObjInToClass.getTitle());
            authors.setText(customerObjInToClass.getAuthors());
            year.setText(customerObjInToClass.getYear());
            genre.setText(customerObjInToClass.getGenres());
            publisher.setText(customerObjInToClass.getPublisher());

        }

        Button sauvgarder= (Button)findViewById(R.id.button);

        final Book finalCustomerObjInToClass = customerObjInToClass;
        //Set on ClickListener
        sauvgarder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DB HELPER
                BookDbHelper DBHElper = new BookDbHelper(BookActivity.this);
                //to write in the database
                SQLiteDatabase db = DBHElper.getWritableDatabase();
                //Create the book
                Book newBook = new Book(name.getText().toString(), authors.getText().toString(), year.getText().toString(), genre.getText().toString(), publisher.getText().toString());

                if (name.getText().toString().equals("")) {
                    //Alert Dialog if the name EditText is Empty
                    AlertDialog dialog = new AlertDialog.Builder(BookActivity.this).create();
                    dialog.setCancelable(false);
                    dialog.setTitle("Sauvgarde Impossible ");
                    dialog.setMessage("Le titre du livre doit être non vide.");

                    dialog.show();

                } else if(name.getText().toString() != ""){
                    if (getIntent().getExtras().getBoolean("AddBook") == true) {
                        //if the book exists then AlerDialog ELSE ADD THE BOOK AND SHOW TOAST
                        if (DBHElper.cursorToBook(DBHElper.fetchBookName(name.getText().toString())).getTitle().equals(name.getText().toString()) ) {
                            AlertDialog dialog = new AlertDialog.Builder(BookActivity.this).create();
                            dialog.setCancelable(false);
                            dialog.setTitle("Ajout impossible ");
                            dialog.setMessage("Un livre portant le même nom existe déjà dans la base de donnée ");
                            dialog.show();

                        } else {
                            DBHElper.addBook(newBook);
                            Toast.makeText(BookActivity.this,
                                    "Added ! ", Toast.LENGTH_LONG).show();

                        }
                    } else {
                        //Updated
                        Book newBookUpdated = new Book(finalCustomerObjInToClass.getId(), name.getText().toString(), authors.getText().toString(), year.getText().toString(), genre.getText().toString(), publisher.getText().toString());
                        int update = DBHElper.updateBook(newBookUpdated);
                        if (update != 0) {
                            Toast.makeText(BookActivity.this,
                                    "Updated ! " + update, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

     }
}
